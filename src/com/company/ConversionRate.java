package com.company;

public class ConversionRate {

    private String ask;
    private String bid;
    private String changes;

    public ConversionRate(String ask, String bid, String changes) {
        this.ask = ask;
        this.bid = bid;
        this.changes = changes;
    }

    public String getAsk() {
        return ask;
    }

    public String getBid() {
        return bid;
    }

    public String getChanges() {
        return changes;
    }
}
