package com.company;
import com.company.ConversionRate;
import com.company.CurrencyApi;

import java.util.concurrent.TimeUnit;
public class Main {

    public static final String ANSI_RESET  = "\u001B[0m";
    public static final String ANSI_RED    = "\u001B[31m";
    public static final String ANSI_GREEN  = "\u001B[32m";

    public static void main(String[] args) {
        CurrencyApi ca = new CurrencyApi();
        ConversionRate cr = null;
        ConversionRate oldCR = null;

        while (true) {
            try {
                Thread.sleep(5000);
                System.out.println("");
                try {
                    if(oldCR == null) {
                        oldCR = ca.fetchData();
                        cr = oldCR;
                    }
                    oldCR = cr;
                    cr = ca.fetchData();

                } catch (Exception e) {}
                if(Double.parseDouble(oldCR.getChanges()) < Double.parseDouble(cr.getChanges())) {
                    System.out.println(ANSI_GREEN +"Ask: " + "\t" + cr.getAsk());
                    System.out.println("Bid: " + "\t" + cr.getBid());
                    System.out.println("Changes: " + "\t" + cr.getChanges());
                } else if(Double.parseDouble(oldCR.getChanges()) > Double.parseDouble(cr.getChanges())) {
                    System.out.println(ANSI_RED + "Ask: " + "\t" + cr.getAsk());
                    System.out.println("Bid: " + "\t" + cr.getBid());
                    System.out.println("Changes: " + "\t" + cr.getChanges());
                } else {
                    System.out.println(ANSI_RESET + "Ask: " + "\t" + cr.getAsk());
                    System.out.println("Bid: " + "\t" + cr.getBid());
                    System.out.println("Changes: " + "\t" + cr.getChanges());
                }
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
